#!/bin/bash

#Small installation script resulting in a sandbox for creating terratest testing scripts for ubuntu 22.04 server.
#https://terratest.gruntwork.io/docs/getting-started/quick-start/

# Install packages needed for sandbox
sudo snap install go --classic
sudo snap install kubectl --classic
sudo snap install terraform --classic
sudo snap install jq
sudo apt update; sudo apt install docker.io docker-compose -y
sudo curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash

# Add user ubuntu to docker group to enable docker for user ubuntu
sudo usermod -aG docker ubuntu

# Clone terratest examples as user ubuntu and run docker terratest example
runuser -l ubuntu -c "git clone https://github.com/gruntwork-io/terratest.git /home/ubuntu/terratest"
runuser -l ubuntu -c "git clone https://gitlab.com/djieno/terratest-examples.git /home/ubuntu/terratest-examples"