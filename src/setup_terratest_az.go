package main

import (
    "fmt"
    "os"
    "os/exec"
)

func main() {
    if os.Geteuid() != 0 {
        fmt.Println("This script needs to be run as root (sudo). Please re-run with sudo.")
        return
    }

    // Install packages needed for sandbox
    executeCommand("snap", "install", "go", "--classic")
    executeCommand("snap", "install", "kubectl", "--classic")
    executeCommand("snap", "install", "terraform", "--classic")
    executeCommand("apt", "update")
    executeCommand("apt", "install", "docker.io", "docker-compose", "jq", "-y")
    executeCommand("bash", "-c", "curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash")

    // Add user ubuntu to docker group to enable docker for user ubuntu
    executeCommand("usermod", "-aG", "docker", "ubuntu")

    // Clone terratest examples as user ubuntu and run docker terratest example
    runUserCommand("ubuntu", "git", "clone", "https://github.com/gruntwork-io/terratest.git", "/home/ubuntu/terratest")
	runUserCommand("ubuntu", "git", "clone", "https://gitlab.com/djieno/terratest-examples.git", "/home/ubuntu/terratest-examples")

}

func executeCommand(command string, args ...string) {
    cmd := exec.Command(command, args...)
    cmd.Stdout = os.Stdout
    cmd.Stderr = os.Stderr
    cmd.Run()
}

func runUserCommand(user string, command string, args ...string) {
    cmd := exec.Command("runuser", "-l", user, "-c", command+" "+argsToCommandString(args...))
    cmd.Stdout = os.Stdout
    cmd.Stderr = os.Stderr
    cmd.Run()
}

func argsToCommandString(args ...string) string {
    cmdStr := ""
    for _, arg := range args {
        cmdStr += arg + " "
    }
    return cmdStr
}
