#!/bin/bash

# clean install script

sudo apt purge -y azure-cli docker.io docker-compose
for i in go kubectl jq terraform ; do sudo snap remove $i; done
rm -fr terratest terratest-examples