#!/bin/bash

clear
echo -en '\n\n' 
echo "Let's install all tools needed to create an terratest environment for a vanilla ubuntu server 22.04"
sleep 3; clear

echo "copy paste"

echo -en '\n\n' 

echo "sudo curl -sL https://gitlab.com/djieno/terratest-examples/-/raw/main/setup-and-terratest-ubuntu-2204.sh?ref_type=heads | sudo bash"
sleep 5
echo -en '\n\n' 

sudo curl -sL https://gitlab.com/djieno/terratest-examples/-/raw/main/setup-and-terratest-ubuntu-2204.sh?ref_type=heads | sudo bash
clear

echo "All tools should be correctly installed...but are they???" 
sleep 5; clear


echo "Let's test that to make sure by some terreatests !"
sleep 5; clear

echo "Change directory into the terratest-example direcotry and excecute some test to validate the installation"
sleep 5; clear

cd terratest-examples; 

echo "Show current path and list the setup-test directory"
sleep 2
pwd  
ls -latr setup-tests
sleep 2
echo -en '\n\n' 

echo "And execute the go test command and point to the tests"
sleep 3
echo -en '\n\n' 

go test -v ./setup-tests
sleep 5
echo -en '\n\n' 

echo "All tests have passed so if the tests are adequate we can assume the environment is correctly setup" 
sleep 5

echo "The end ...."
