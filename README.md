# Terratest-examples

## Description
This repos contains some installation or help with running terratest scripts from a vanilla Ubuntu server. The starting point is a clean untouched Ubuntu 22.04 server and install all needed applications to run terratest using terraform, docker, docker-compose, azcli tools and go with all needed modules. 

[![asciicast](https://asciinema.org/a/601507.svg)](https://asciinema.org/a/605580)

## Installation
Use any vanilla Ubuntu 22.04 amd64 (virtual) machine to create a sandbox. Simply copy the line blow in a ubuntu (virtual) machine.
```
sudo curl -sL https://gitlab.com/djieno/terratest-examples/-/raw/main/setup-and-terratest-ubuntu-2204.sh?ref_type=heads | sudo bash
```

## Run some test on the installation script
```
cd terratest-examples
go test -v ./setup-tests
```

outputs something like this
```
=== RUN   TestAzureCLExecution
--- PASS: TestAzureCLExecution (0.81s)
=== RUN   TestGoInstallation
--- PASS: TestGoInstallation (0.13s)
=== RUN   TestGoVersion
--- PASS: TestGoVersion (0.00s)
=== RUN   TestJQExecution
--- PASS: TestJQExecution (0.34s)
PASS
ok  	gitlab.com/djieno/terratest-examples.git/setup-tests	(cached)
```

## Support
Open an issue on this repo

## Roadmap
No direct plans.

## Contributing
Just create an issue and PR if you'd like to add / change.

## License
GNU 3.0

## Project status
Just create the initial steps and not fully done with the test examples.
