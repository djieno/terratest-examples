#!/bin/bash

#Small installation script and execution of a terraform aks creation with an nginx webpage - operated by terratest and perform a webpage test.
# This Prerequisites below to have executed succesffully (you can execute the line below again if you need to)
# sudo curl -sL https://gitlab.com/djieno/terratest-examples/-/raw/main/setup-and-terratest-ubuntu-2204.sh?ref_type=heads | sudo bash

#https://github.com/gruntwork-io/terratest/tree/master/examples/azure/terraform-azure-aks-example

# some variables
AZ_AKS_DIR=$HOME/terratest/examples/azure/terraform-azure-aks-example

# validate the current user is ubuntu
if [ "$USER" != "ubuntu" ]
  then
        echo "This script must be run as user ubuntu" 1>&2
        exit 1
fi

# login to azure environment with sufficient privileges (this example is made a free azure introduction setup)
if [ ! -f $HOME/azure.loggedin ]
  then
        echo "Please login to Azure environment with sufficient privileges"
        az login # Copy the code and past it into microsoft.com/devicelogin, select correct account, ack cli login
        touch $HOME/azure.loggedin
fi

# set the needed aks creds
export ARM_SUBSCRIPTION_ID="$(az account list --query "[?isDefault].id" -o tsv)"

# set a sp for app if needed
if [ ! -f $HOME/sp_output.json ]
  then
        echo "Creating service principle and create an appId"
        az ad sp create-for-rbac > $HOME/sp_output.json
        export TF_VAR_client_id=$(cat $HOME/sp_output.json |jq --raw-output .appId)
        export TF_VAR_client_secret=$(cat $HOME/sp_output.json |jq --raw-output .password)
    else
        echo "Reusing service principle and create an appId"
        export TF_VAR_client_id=$(cat $HOME/sp_output.json |jq --raw-output .appId)
        export TF_VAR_client_secret=$(cat $HOME/sp_output.json |jq --raw-output .password)
fi

# create a ssh-key for login and required for aks creation
ssh-keygen -t rsa -N '' -f ~/.ssh/id_rsa <<< y

# add sensitive = yes to the $AZ_AKS_DIR/output.tf for client_key
if [ ! -f $AZ_AKS_DIR/output.tf.done ]
  then
        echo "Adding 'sensitive = yes' under client_key in file $AZ_AKS_DIR/output.tf"
        sed -i '/client_key$/a \ \ sensitive = true' $AZ_AKS_DIR/output.tf
        touch $AZ_AKS_DIR/output.tf.done
fi

# enter the correct test directory
cd ~/terratest/test/azure/

# create aks and deploy aks and test webpage
go test -v -timeout 60m -tags azure -run TestTerraformAzureAKS

# Remove created app by idG from azure
az ad app delete --id $TF_VAR_client_id
rm $HOME/sp_output.json