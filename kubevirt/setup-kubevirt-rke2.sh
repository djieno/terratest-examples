
#install kubevirt operator
export VERSION=$(curl -s https://storage.googleapis.com/kubevirt-prow/release/kubevirt/kubevirt/stable.txt)
echo $VERSION
kubectl create -f "https://github.com/kubevirt/kubevirt/releases/download/${VERSION}/kubevirt-operator.yaml"

#install cdi
export VERSION=$(basename $(curl -s -w %{redirect_url} https://github.com/kubevirt/containerized-data-importer/releases/latest))
kubectl create -f https://github.com/kubevirt/containerized-data-importer/releases/download/$VERSION/cdi-operator.yaml
kubectl create -f https://github.com/kubevirt/containerized-data-importer/releases/download/$VERSION/cdi-cr.yaml

# verify all is working
kubectl get pods -n cdi

# deploy vm
kubectl apply -f https://gitlab.com/djieno/terratest-examples/-/raw/main/kubevirt/kubevirt-ubuntu-2204-server.yaml?ref_type=heads

# install virtctl using krew

# connect to console
kubectl virt console ubuntuvm # see userdata login: ubuntu password: passwd

#exit console: CTRL+] 

# connect to nodeport svc
# check nodeport svc port: kubectl get svc
# as example:
# ubuntuvm-ssh             NodePort    10.43.135.19   <none>        20222:30001/TCP   3m5
#
# k8s-host-ip = INTERNAL-IP 
# kubectl get no -o wide
# as example:
# NAME     STATUS   ROLES                       AGE   VERSION          INTERNAL-IP   EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION     CONTAINER-RUNTIME
# node02   Ready    control-plane,etcd,master   8d    v1.31.4+rke2r1   10.1.1.99    <none>        Ubuntu 24.04.1 LTS   6.8.0-51-generic   containerd://1.7.23-k3s2

ssh ubuntu@10.1.1.99 -p 30001 -i [the public key in the virtmachine block]
