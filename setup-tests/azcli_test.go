package test

import (
	// "fmt"
	"os/exec"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAzureCLExecution(t *testing.T) {
	// Command and arguments
	cmd := exec.Command("az", "--version")

	output, err := cmd.CombinedOutput()
	if err != nil {
		t.Fatalf("Error executing Azure CLI command: %v", err)
	}

	expectedOutputSubstring := "azure-cli" // A substring expected in the version output

	assert.Contains(t, string(output), expectedOutputSubstring, "Azure CLI version output should contain the expected substring")
}
