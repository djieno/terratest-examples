package test

import (
	// "fmt"
	"os/exec"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestJQExecution(t *testing.T) {
	// Sample JSON input
	jsonInput := `{"name": "John", "age": 30}`

	// Command and arguments
	cmd := exec.Command("jq", ".name")
	cmd.Stdin = strings.NewReader(jsonInput)

	output, err := cmd.Output()
	if err != nil {
		t.Fatalf("Error executing jq command: %v", err)
	}

	expectedOutput := "\"John\"\n" // The expected output for the given JSON input

	assert.Equal(t, expectedOutput, string(output), "JQ output should match the expected output")
}
