package test

import (
	"os"
	"os/exec"
	"testing"
)

func TestGoInstallation(t *testing.T) {
	// Create a temporary directory to hold the test program
	tempDir := t.TempDir()

	// Create a simple "Hello, World!" Go program
	helloWorldCode := `
package main

import "fmt"

func main() {
    fmt.Println("Hello, World!")
}
`
	// Write the code to a file
	testFile := tempDir + "/test.go"
	err := os.WriteFile(testFile, []byte(helloWorldCode), 0644)
	if err != nil {
		t.Fatalf("Error writing test code to file: %v", err)
	}

	// Compile and run the test program
	cmd := exec.Command("go", "run", testFile)

	output, err := cmd.CombinedOutput()
	if err != nil {
		t.Fatalf("Error compiling or running test program: %v\nOutput:\n%s", err, output)
	}

	expectedOutput := "Hello, World!\n"

	if string(output) != expectedOutput {
		t.Fatalf("Unexpected output:\nExpected: %s\nActual: %s", expectedOutput, output)
	}
}
