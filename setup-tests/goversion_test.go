package test

import (
	"fmt"
	"log"
	"os/exec"
	"regexp"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGoVersion(t *testing.T) {
	out, err := exec.Command("go", "version").Output()
	if err != nil {
		log.Fatal(err)
	}

	actualVersion := string(out)
	// fmt.Printf("Actual Version: %s\n", actualVersion)

	versionPattern := `go\d+\.\d+\.\d+`
	versionRegex := regexp.MustCompile(versionPattern)
	matches := versionRegex.FindStringSubmatch(actualVersion)

	if len(matches) < 1 {
		log.Fatal("Failed to extract version from actual output.")
	}

	actualVersionNumber := matches[0]

	expectedVersion := "go1.18.0"
	assert.True(t, versionIsGreaterOrEqual(actualVersionNumber, expectedVersion), "Actual version is not greater than or equal to expected version.")
}

func versionIsGreaterOrEqual(actualVersion, expectedVersion string) bool {
	actualParts := parseVersion(actualVersion)
	expectedParts := parseVersion(expectedVersion)

	for i := 0; i < len(expectedParts); i++ {
		if i >= len(actualParts) {
			return false
		}
		if actualParts[i] > expectedParts[i] {
			return true
		} else if actualParts[i] < expectedParts[i] {
			return false
		}
	}

	return true
}

func parseVersion(version string) []int {
	versionParts := strings.Split(version, ".")
	parsedParts := make([]int, len(versionParts))
	for i, part := range versionParts {
		fmt.Sscanf(part, "%d", &parsedParts[i])
	}
	return parsedParts
}