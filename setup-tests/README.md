# Test-terraform-aks-nginx

## Description
This repos contains some installation or help with running terratest scripts from a vanilla Ubuntu server. The starting point is a clean untouched Ubuntu 22.04 server and install all needed applications to run terratest using terraform, docker, docker-compose, azcli tools and go with all needed modules. 

This test we're going to perform an installation of an new aks cluster using terraform operated by terratest. 

## Prerequisites
1 clean ubuntu 22.04 with command below executed successfully

```
sudo curl -sL https://gitlab.com/djieno/terratest-examples/-/raw/main/setup-and-terratest-ubuntu-2204.sh?ref_type=heads | sudo bash
```

## Run some test on the installation script
```
cd terratest-examples
go test -v ./setup-tests
```

outputs something like this
```
=== RUN   TestAzureCLExecution
--- PASS: TestAzureCLExecution (0.81s)
=== RUN   TestGoInstallation
--- PASS: TestGoInstallation (0.13s)
=== RUN   TestGoVersion
--- PASS: TestGoVersion (0.00s)
=== RUN   TestJQExecution
--- PASS: TestJQExecution (0.34s)
PASS
ok  	gitlab.com/djieno/terratest-examples.git/setup-tests	(cached)
